<h1 align="center">Welcome to Videobox 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: GPL V2" src="https://img.shields.io/badge/License-GPLv2-yellow.svg" />
  </a>
</p>

> A youtube video player written in rust that uses the invidious api.
>:zap: This is alpha software so expect bugs and a not polished UI ! :zap:
------
![alt text](screenshot.png "Videobox with a running video") [^1]
[^1]: All images belong to the blender foundation and are licensed under the creative commons license. **The UI will not stay the same and will probably change.**
------
## Features
- [x] Subscriptions
- [x] Search
- [x] config support
  - [ ] Support random instance
- [ ] Get a logo
- [ ] Favorites
- [ ] Remove Search tab and put it in to the header bar
- [ ] Ability to import and export subscriptions
- [ ] Add settings interface for app.toml
- [ ] Responsive Layout for Pinephone and Librem5
- [ ] Refactor Code
- [ ] Build solid tests
- [ ] Builds
  - [ ] Gitlab CI
  - [ ] Aur
  - [ ] Alpine

------
## Install
[For now you have to build the application from source but builds are planned.](#Build Instructions)

------
## Build Instructions
### Requirements
- [Rust](https://www.rust-lang.org/)
- [libmpv](https://github.com/mpv-player/mpv)
- [gtk](https://www.gtk.org/)
### Building
1. First clone this repo
``` sh
git clone https://gitlab.com/techfreak/videobox
```
2. Build the application
``` sh
cd videobox
cargo build --release --locked
```
3. Run it
``` sh
./target/release/videobox
```
4. If you made changes to the source code run it with:
``` sh
cargo run
```
5. **Profit**

------
## Open Source Libaries used

:heart: Huge thanks to the guys for proving these awesome libraries.

- [reqwest](https://github.com/seanmonstar/reqwest)
- [serde](https://github.com/serde-rs/serde)
- [futures-rs](https://github.com/rust-lang/futures-rs)
- [gtk-sys](https://github.com/gtk-rs/sys)
- [gtk-rs](https://github.com/gtk-rs/gtk)
- [gdk](https://github.com/gtk-rs/gdk)
- [gdk-pixbuf](https://github.com/gtk-rs/gdk-pixbuf)
- [bytes](https://github.com/tokio-rs/bytes)
- [toml](https://github.com/alexcrichton/toml-rs)
- [dirs](https://github.com/soc/dirs-rs)
- [mpv-rs](https://github.com/Cobrand/mpv-rs)

-----
## Authors
👤 **techfreak**

-----

## Show your support
- Give a ⭐️ if this project helped you!
- Create a logo for the project
- Write an issue with your idea or feature request
- Make a pull request with your changes
- Write me on riot.im in the invidious channel
- Donate me money or donate for a feature you would like to get implemented

-----
## License
This project is licensed under the GPLv2 - see the [LICENSE.md](LICENSE.md) file for details

-----

