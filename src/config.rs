/*
Copyright (C) 2019  techfreak
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::io::{BufReader, Read, Write};

//static mut CONFIG: Option<config::AppConfig> = None;
#[allow(dead_code)]
pub mod config {
    use serde::{Deserialize, Serialize};
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct AppConfig {
        pub app: App,
        pub instance: Instance,
    }
    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub struct App {
        pub show_favorites: bool,
        pub subs: Vec<String>,
    }
    #[derive(Serialize, Deserialize, Debug, Clone)]

    pub struct Instance {
        pub url: String,
    }
}

impl config::AppConfig {
    pub fn new() -> config::AppConfig {
        config::AppConfig {
            app: config::App {
                show_favorites: true,
                subs: vec![],
            },
            instance: config::Instance {
                url: "invidio.us".to_string(),
            },
        }
    }
    #[allow(dead_code)]
    pub fn modify_subs(mut self, subs: Vec<String>) {
        self.app.subs = subs;
    }
}

#[allow(dead_code)]
pub fn get_config_folder() -> String {
    format!(
        "{}{}",
        dirs::config_dir().unwrap().to_str().unwrap(),
        "/videobox/"
    )
}
#[allow(dead_code)]
pub fn get_app_file() -> String {
    format!("{}{}", get_config_folder(), "app.toml")
}

#[allow(dead_code)]
pub fn get_tmp_folder() -> String {
    format!("{}{}", dirs::cache_dir().unwrap().display(), "/videobox/")
}

#[allow(dead_code)]
pub fn get_img_folder() -> String {
    format!("{}{}", get_tmp_folder(), "/thumbnails/")
}

#[allow(dead_code)]
fn load_config() -> Result<config::AppConfig, Box<dyn std::error::Error>> {
    let app_folder: &str = &get_config_folder();
    let app_config_file: &str = &get_app_file();
    let app_config: Option<config::AppConfig>;
    if !std::path::Path::new(&app_config_file).exists() {
        std::fs::create_dir_all(app_folder)?;
        app_config = Some(config::AppConfig::new());
        let mut buffer = std::fs::File::create(&app_config_file)?;
        let config_format = format!(
            "{}{}",
            "# Configuartion file for Videobox\n",
            toml::to_string(&app_config).unwrap()
        );
        buffer.write_all(config_format.as_bytes())?;
    } else {
        let file = std::fs::File::open(&app_config_file)?;
        let mut buf_reader = BufReader::new(file);
        let mut contents = String::new();
        buf_reader.read_to_string(&mut contents)?;
        app_config = Some(toml::from_str(&contents).unwrap());
    }
    Ok(app_config.unwrap())
}

#[allow(dead_code)]
pub fn update_config(new_config: config::AppConfig) -> Result<(), Box<dyn std::error::Error>> {
    //let bla_config = toml::to_string(&new_config).unwrap();
    let mut buffer = std::fs::File::open(&get_app_file())?;
    buffer.write_all(toml::to_string(&new_config).unwrap().as_bytes())?;
    Ok(())
}

#[allow(dead_code)]
pub fn get_config() -> Result<config::AppConfig, Box<dyn std::error::Error>> {
    Ok(load_config()?)
}
