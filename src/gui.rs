/*
   Copyright (C) 2019  techfreak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */
extern crate futures;
extern crate gdk_pixbuf;
extern crate gtk;
extern crate tokio;
extern crate gdk;
extern crate mpv;
use gdk_pixbuf::Pixbuf;
use gtk::prelude::*;
use std::io::Write;
use tokio::runtime::current_thread::Runtime;

mod config;
mod lib;

/// Downloads an image asynchronously from a given url to a given file location.
async fn download_img(img_url: &str, file_path: String) -> Result<(), Box<dyn std::error::Error>> {
    let mut buffer = std::fs::File::create(&file_path)?;
    buffer.write_all(&lib::get_image(&img_url).await?.unwrap())?;
    Ok(())
}

/// Downloads images asynchronously when they are not stored on the disk.                     
fn cache_imgs<T: lib::Video>(videos: &std::vec::Vec<T>) -> Result<(), std::io::Error> {
    let app_path: &str = &config::get_img_folder();
    std::fs::create_dir_all(config::get_tmp_folder())?;
    std::fs::create_dir_all(app_path)?; //todo move to a setup function
    let mut tmp = Vec::new();
    for video in videos.iter() {
        let thumbnail: String = format!("{}{}.jpg", app_path, video.get_video_id());
        if !std::path::Path::new(&thumbnail).exists() {
            tmp.push(download_img(video.get_video_url(4), thumbnail));
        }
    }
    let mut current_thread = Runtime::new()?;
    &current_thread.block_on(futures::future::join_all(tmp));
   Ok(())
}

///Plays a youtube video when user clicked on the video box.
fn video_got_clicked(event_box: &gtk::EventBox) -> gtk::Inhibit {
    let video_id = gtk::WidgetExt::get_widget_name(event_box).unwrap().to_string();

    let mut current_thread = Runtime::new().unwrap();
    let meta_data = &current_thread.block_on(lib::get_video_by_id(&video_id)).unwrap();
    let video_url = meta_data.adaptive_formats[2].url.clone();
    let audio_url = meta_data.adaptive_formats[meta_data.adaptive_formats.len()-1].url.clone();
    let title = meta_data.title.clone();
    //println!("{}", video_url);
    //println!("audio: {}",audio_url);
    //gst::init().unwrap();

    std::thread::spawn(move || {  
      let ret: Result<(), Box<dyn std::error::Error>> = (|| {
        let mut mpv_builder = mpv::MpvHandlerBuilder::new()?;
        mpv_builder.set_option("sid","no")?;
        mpv_builder.set_option("osc",true)?;
        let mut mpv = mpv_builder.build()?;

        mpv.command(&["loadfile",&video_url,"replace",&format!("audio-file=\"{}\"",audio_url)])?;
        mpv.set_property("title",format!("{}",title).as_ref())?;
        mpv.set_property("loop","0")?;
        mpv.set_property("speed",1.0)?;


        'main: loop {
          while let Some(event) = mpv.wait_event(0.0) {
            println!("RECEIVED EVENT : {:?}", event);
            match event {
              mpv::Event::Shutdown | mpv::Event::Idle => {
                break 'main;
              }
              _ => {}
            };
          }
        }
        Ok(()) 
      })();
      ret.unwrap();
    });
    gtk::Inhibit(true)
}
///Populates the subscriptions tab with videos from the stored channels from the config database.
fn display_subscriptions(subs_box: gtk::Box) -> Result<(), Box<dyn std::error::Error>> {
  let (mut fetch_channels, mut timeline) = (Vec::new(), Vec::new());

  let app_config = &config::get_config()?.clone();
  let config_subs_id: Vec<String> = app_config.app.subs.clone();
  //config_subs_id.push("UChkVOG0PqkXIHHmkBGG15Ig".to_string());
  //app_config.modify_subs(config_subs_id.clone());
  //config::update_config(app_config.clone())?;
  for item in config_subs_id.iter() {
    fetch_channels.push(lib::get_channel_by_id(item.clone()));
  }
  let mut current_thread = Runtime::new()?;
  let subs = &current_thread.block_on(futures::future::join_all(fetch_channels));

  for channel in subs.clone() {
    match &channel {
      Ok(v) => timeline.extend(&v.latest_videos),
      Err(_) => println!("{:?}", channel),
    };
  }

  if !timeline.is_empty() {
    cache_imgs(&timeline).unwrap();
    timeline.sort_by_key(|x| std::cmp::Reverse(x.published));
    println!("{:?}", app_config.instance);
    for video in timeline.iter() {
      let event_box = &gtk::EventBox::new();
      let card = &gtk::Fixed::new();
      let image: gtk::Image = gtk::Image::new();
      let img_path: &str = &format!("{}{}.jpg", config::get_img_folder(), &video.video_id);
      image.set_from_pixbuf(Some(&Pixbuf::new_from_file(img_path)?));
      image.set_size_request(200, 200);
      gtk::WidgetExt::set_widget_name(event_box, &video.video_id);

      let title: gtk::Label = gtk::Label::new(Some(&video.title));
      let views: gtk::Label =
        gtk::Label::new(Some(&format!("Views: {}", video.view_count.to_string())));
      let author_and_date: gtk::Label = gtk::Label::new(Some(&format!(
            "{} - {}",
            &video.author, &video.published_text
      )));

      //css ids
      gtk::WidgetExt::set_widget_name(&image, "video_thumbnail");
      gtk::WidgetExt::set_widget_name(&title, "video_title");
      gtk::WidgetExt::set_widget_name(&author_and_date, "published_date");

      //css classes
      card.put(&image, 10, 10);
      card.put(&title, 360, 20);
      card.put(&author_and_date, 360, 40);
      card.put(&views, 360, 60);
      card.set_size_request(-1, 100);

      event_box.add(card);
      event_box.connect_button_press_event(move |event_box, _| {
        video_got_clicked(event_box)
      });
      subs_box.pack_start(event_box, true, true, 0);
    }
    subs_box.show_all();
  }
  Ok(())
}

fn clear_box(input_box: &gtk::Box) {
  for previous_box in input_box.get_children() {
    previous_box.destroy();
  }
}

//Populates the search tab with videos from the search query.
fn display_search(builder:&gtk::Builder,entry:&gtk::SearchEntry){
  let search_box: gtk::Box = builder.get_object("search_box").unwrap();

  let mut current_thread = Runtime::new().unwrap();
  let query: &str = &entry.get_text().unwrap().to_string();
  let query_results = &current_thread.block_on(lib::search_video_title(query));
  let res: &Vec<lib::api_search_video_scheme::Video> =
    &query_results.clone().as_ref().unwrap();

  cache_imgs(res).unwrap();
  clear_box(&search_box);

  for video in res.iter() {
    let event_box = &gtk::EventBox::new();
    let card = &gtk::Fixed::new();
    let title: gtk::Label = gtk::Label::new(Some(&video.title));
    let views: gtk::Label =
      gtk::Label::new(Some(&format!("Views: {}", video.view_count.to_string())));
    let author_and_date: gtk::Label = gtk::Label::new(Some(&format!(
          "{} - {}",
          &video.author, &video.published_text
    )));
    let image: gtk::Image = gtk::Image::new();
    let img_path: &str = &format!("{}{}.jpg", config::get_img_folder(), &video.video_id);

    gtk::WidgetExt::set_widget_name(event_box, &video.video_id);

    event_box.connect_button_press_event(move |event_box, _| {
      video_got_clicked(event_box)
    });

    image.set_from_pixbuf(Some(&Pixbuf::new_from_file(img_path).unwrap()));
    image.set_size_request(200, 200);
    card.put(&image, 0, 0);
    card.put(&title, 370, 20);
    card.put(&views, 370, 60);
    card.put(&author_and_date, 370, 40);
    card.set_size_request(-1, 100);
    event_box.add(card);
    search_box.pack_start(event_box, true, true, 0);
  }
  search_box.show_all();
}

///Clears the thumbnails folder of it images.
fn clear_cache() -> Result<(), Box<dyn std::error::Error>>{
    let tmp = config::get_tmp_folder()+"thumbnails/";
    let entries = std::fs::read_dir(tmp)?
        .map(|res| res.map(|e| tokio::fs::remove_file(e.path())))
        .collect::<Result<Vec<_>, std::io::Error>>()?;
    let mut current_thread = Runtime::new()?;
    &current_thread.block_on(futures::future::join_all(entries));
    Ok(())
}


///Sets up the ui and connects events
fn main() -> Result<(), Box<dyn std::error::Error>> {
    //disable locale otherwise mpv complains
    unsafe {                                                                              
        gtk_sys::gtk_disable_setlocale();                                                 
    }       

    gtk::init()?;

    let glade_src = include_str!("../gui.glade");
    let builder = gtk::Builder::new_from_string(glade_src);
    let main_window: gtk::Window = builder.get_object("main_window").unwrap();
    main_window.connect_delete_event(move |_, _| {
        clear_cache().unwrap();
        gtk::main_quit();
        Inhibit(false)
    });

    let vbox: gtk::Box = builder.get_object("box").unwrap();
    let subs_stack: gtk::Stack = builder.get_object("subs").unwrap();
    let search_stack: gtk::Stack = builder.get_object("search").unwrap();
    let favorites_stack: gtk::Stack = builder.get_object("favorites").unwrap();
    let switcher: gtk::StackSwitcher = builder.get_object("switcher").unwrap();
    let search_field: gtk::SearchEntry = builder.get_object("search_field").unwrap();
    let menu_bar: gtk::HeaderBar = builder.get_object("bar").unwrap();

    menu_bar.set_show_close_button(true);
    subs_stack.add_titled(&search_stack, "search", "Search");
    subs_stack.add_titled(&favorites_stack, "fav", "Favorites");
    switcher.set_stack(Some(&subs_stack));
    vbox.pack_start(&subs_stack, true, true, 0);
    main_window.show_all();

    let sub_box: gtk::Box = builder.get_object("sub_box").unwrap();
    display_subscriptions(sub_box).unwrap();
    search_field.connect_activate(move |entry| {
        display_search(&builder,&entry);
    });
    gtk::main();

    Ok(())
}
